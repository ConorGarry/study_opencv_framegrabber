package com.example.study_ffmpegframegrabber;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.Button;

import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.avutil;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.FrameRecorder;

import java.io.File;


//import org.bytedeco.javacpp.*;


//import static org.bytedeco.javacpp.opencv_core.*;



public class MainActivity extends Activity {
	private static final String TAG = "MainActivity";

	public static final String DIRECTORY_CAMERA = "/DCIM/Camera";
	public static final String TEST_FOOTAGE = "/VID_20161117_140337.mp4";

	private Button btn_edit, btn_save;
	private SurfaceView sv_playback;

//	private static String videoPath = "/mnt/sdcard/new_stream.mp4";
	private static String videoPath = Environment.getExternalStorageDirectory() + DIRECTORY_CAMERA + TEST_FOOTAGE;
	private static String outputPath = Environment.getExternalStorageDirectory() + DIRECTORY_CAMERA + "/output.flv";

	FrameGrabber grabber;
	FrameRecorder recorder;

	private int width, height;
	private int sampleAudioRateInHz = 44100;
	private int frameRate = 60;
	
//	static CanvasFrame canvasFrame = new CanvasFrame("CAM");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Log.d(TAG, "onCreate: created");
		File f = new File(videoPath);
		Log.d(TAG, "File: " + f + " Exists: " + f.exists());

		try {
			grabber = FrameGrabber.createDefault(videoPath);
		} catch (Exception e1) {
			Log.e(TAG, "Create FrameGrabber failed");
			e1.printStackTrace();
		}
//		try {
//
//			grabber.start();
//			Log.d(TAG, "Grabber Started");
//		} catch (Exception e) {
//			Log.e(TAG, "Grabber Not Started");
//			e.printStackTrace();
//		}
//
//		width = grabber.getImageWidth();
//		height = grabber.getImageHeight();

//		initRecorder(outputPath);

//		captureFrames();

//		try {
//			recorder.stop();
//			Log.d(TAG, "Recorder stopped");
//		} catch (org.bytedeco.javacv.FrameRecorder.Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		try {
//			grabber.stop();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Log.d(TAG, "Grabber stopped");
//
//		convert();
	}

	private void initRecorder(String path) {
		try {
			recorder = FrameRecorder.createDefault(path, width, height);
		} catch (org.bytedeco.javacv.FrameRecorder.Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		recorder.setSampleRate(sampleAudioRateInHz);
		// re-set in the surface changed method as well
		recorder.setFormat("flv");
		recorder.setFrameRate(grabber.getFrameRate());
		recorder.setVideoBitrate(10 * 1040 * 1040);
		recorder.setVideoCodec(avcodec.AV_CODEC_ID_FLV1);
        recorder.setPixelFormat(avutil.AV_PIX_FMT_YUV420P);
       
       
		try {
			recorder.start();
			Log.d(TAG, "Recorder started.");
		} catch (org.bytedeco.javacv.FrameRecorder.Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	private void captureFrames() {
//		Frame frame = null;
//		try {
//			while ((frame = grabber.grabFrame()) != null) {
//				IplImage source = frame.image;
//				if (source != null) {
//					Bitmap overlayGrab = null;
//			    	overlayGrab = Bitmap.createBitmap(width, height, Bitmap.Config.ALPHA_8);
//					IplImage destination = IplImage.create(width, height, source.depth(), source.nChannels());
//					frame.image = destination;
//					recorder.record(frame);
//				}
//
//			}
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (org.bytedeco.javacv.FrameRecorder.Exception e) {
//			Log.e(TAG, "FrameRecorder Failed");
//			e.printStackTrace();
//		}
//	}

	public void convert() {

	    FrameGrabber frameGrabber = null;
		try {
			frameGrabber = FrameGrabber.createDefault(videoPath);
		} catch (Exception e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

	    Frame captured_frame = null;

	    FrameRecorder recorder = null;
//	    recorder = new FFmpegFrameRecorder(outputPath, width, height, 1);
	    try {
			recorder = FrameRecorder.createDefault(outputPath, width, height);
		} catch (org.bytedeco.javacv.FrameRecorder.Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	    recorder.setVideoCodec(avcodec.AV_CODEC_ID_MPEG4);
	    recorder.setFrameRate(frameGrabber.getFrameRate());
	    recorder.setFormat(frameGrabber.getFormat());
	    try {
	        recorder.start();
	        frameGrabber.start();
	        while (true) {
	            try {
	                captured_frame = frameGrabber.grab();

	                if (captured_frame == null) {
	                    System.out.println("!!! Failed cvQueryFrame");
	                    break;
	                }
	                recorder.record(captured_frame);
	            } catch (Exception e) {
	            }
	        }
	        recorder.stop();
	        recorder.release();
	    } catch (FrameRecorder.Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
	        e.printStackTrace();
	    }
	}



}
